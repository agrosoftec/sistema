<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class usuario extends CI_Controller {
    
        public function __construct() {
            parent::__construct();
            //identifica qual aplicação esta tentando acessar
            $aplicacao = '/'.$this->router->fetch_class().'/'.$this->router->fetch_method();
            $this->dados_acesso = $this->usuariopermissao->verificarAcesso($aplicacao);
        }
        
        public function usuarios(){
            $crud = new Grocery_CRUD();
            
            $crud->set_table("cadusuario");
            $crud->set_subject("Usuario");
            
            $crud->unique_fields("login");
            
            $crud->columns('nome','login','senha','grupo');
            $crud->fields('nome','login','senha','grupo');
            $crud->required_fields('nome','login','senha','grupo');
            $crud->field_type('senha', 'password');
            
            $crud->display_as("nome", "Nome");
            $crud->display_as("login", "Login");
            $crud->display_as("senha", "Senha");
            $crud->display_as("grupo", "Grupo");
            
            $crud->set_relation("grupo", "grupousuario", "descricao");
            
            $crud->unset_clone();
            
            //permissoes do usuario
            if ($this->dados_acesso['alterar'] == "0"){
                $crud->unset_edit();
            }
            if ($this->dados_acesso['deletar'] == "0"){
                $crud->unset_delete();
            }
        
            $output = $crud->render();    
        
            $this->template->load("layout/painel", "usuario/viewUsuario", (array) $output);
        }
        
        public function grupos(){
            $crud = new Grocery_CRUD();
            
            $crud->set_table("grupousuario");
            $crud->set_subject("Grupo de Usuario");
            
            $crud->unique_fields("descricao");
            
            $crud->columns('descricao');
            $crud->fields('descricao');
            $crud->required_fields('descricao');
            
            $crud->display_as("descricao", "Descrição");
            
            $crud->unset_clone();
            
            //permissoes do usuario
            if ($this->dados_acesso['alterar'] == "0"){
                $crud->unset_edit();
            }
            if ($this->dados_acesso['deletar'] == "0"){
                $crud->unset_delete();
            }
        
            $output = $crud->render();    
        
            $this->template->load("layout/painel", "usuario/viewGrupousu", (array) $output);
        }
        
        public function permissaoGrupo(){
            $crud = new Grocery_CRUD();
            
            $crud->set_table("cadpermissao");
            $crud->set_subject("Permissões");
            
//            $crud->unique_fields(array('grupo','aplicacao'));
            
            $crud->columns('grupo','aplicacao','acessar','alterar','deletar','ativo');
            $crud->fields('grupo','aplicacao','acessar','alterar','deletar','ativo');
            $crud->required_fields('grupo','aplicacao','acessar','alterar','deletar','ativo');
            
            $crud->display_as("grupo", "Grupo");
            $crud->display_as("aplicacao", "Aplicação");
            $crud->display_as("acessar", "Acessar");
            $crud->display_as("alterar", "Alterar");
            $crud->display_as("deletar", "Deletar");
            $crud->display_as("ativo", "Ativo");
            
            $crud->set_relation("grupo", "grupousuario", "descricao");
            $crud->set_relation("ativo", "cadstatus", "nome");
            $crud->set_relation("aplicacao", "cadaplicacoes", "descricao");
            
            $lista_permissao = array('0' => 'Não', '1' => 'Sim');
            $crud->field_type('acessar', 'dropdown', $lista_permissao);
            $crud->field_type('alterar', 'dropdown', $lista_permissao);
            $crud->field_type('deletar', 'dropdown', $lista_permissao);
            
            $crud->unset_clone();
            
            //permissoes do usuario
            if ($this->dados_acesso['alterar'] == "0"){
                $crud->unset_edit();
            }
            if ($this->dados_acesso['deletar'] == "0"){
                $crud->unset_delete();
            }
        
            $output = $crud->render();    
        
            $this->template->load("layout/painel", "usuario/viewGrupousu", (array) $output);
        }
}