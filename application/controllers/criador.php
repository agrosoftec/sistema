<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class criador extends CI_Controller  {
    
    public function __construct() {
            parent::__construct();
            //identifica qual aplicação esta tentando acessar
            $aplicacao = '/'.$this->router->fetch_class().'/'.$this->router->fetch_method();
            $this->dados_acesso = $this->usuariopermissao->verificarAcesso($aplicacao);
    }

    
    public function criadores(){
        $crud = new grocery_CRUD();
        
        $crud->set_table('cadcriador');
        $crud->columns('nome','cnpj', 'telefone','email');
        
        $crud->set_subject("Criador");
        
        $crud->display_as("codcri", "ID");
        $crud->display_as("nome", "Nome");
        $crud->display_as("cnpj", "CNPJ");
        $crud->display_as("endereco", "Endereço");
        $crud->display_as("municipio", "Municipio");
        $crud->display_as("uf", "UF");
        $crud->display_as("cep", "CEP");
        $crud->display_as("telefone", "TEL");
        $crud->display_as("email", "E-mail");
        $crud->display_as("dtcadastro", "Data Cad");
        $crud->display_as("status", "Status");
        //permitir apenas números
        $crud->field_type("cnpj", "integer");
        $crud->field_type("cep", "integer");
        $crud->field_type("telefone", "integer");
        
        $crud->unset_clone();
        
        //permissoes do usuario
        if ($this->dados_acesso['alterar'] == "0"){
            $crud->unset_edit();
        }
        if ($this->dados_acesso['deletar'] == "0"){
            $crud->unset_delete();
        }
        
        $crud->unique_fields('cnpj');
        
        $crud->required_fields("nome","cnpj","endereco","municipio","uf","cep","telefone","email","dtcadastro","status");
        
//        $crud->set_relation("status", "cadstatus", "nome");
//        $crud->set_relation("municipio", "municipios", "nome");
//        $crud->set_relation("uf", "estados", "uf");
        
        $output = $crud->render();
        $this->template->load("layout/painel", "criador/cadastroCriador", (array) $output);
    }
    
}
