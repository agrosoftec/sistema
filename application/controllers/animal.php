<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class animal extends CI_Controller {
    
    public function __construct() {
            parent::__construct();
            //identifica qual aplicação esta tentando acessar
            $aplicacao = '/'.$this->router->fetch_class().'/'.$this->router->fetch_method();
            $this->dados_acesso = $this->usuariopermissao->verificarAcesso($aplicacao);
    }
    
    public function animais(){
        $crud = new grocery_CRUD();
        
        $crud->set_table('cadgado');
        $crud->columns('ncontrole','nome','codraca','codcategoria','codpai','codmae','codfazenda');
        $crud->fields('ncontrole','nome','codraca','codcategoria','dtnascimento','sexo','pesonascimento','codpelagem','codcriador','codpai','codmae','codfazenda','status','desmamado','dataDesmame');
        
        $crud->set_subject("Animal");
        
        $crud->display_as("codgado", "ID");
        $crud->display_as("nome", "Nome");
        $crud->display_as("ncontrole", "Nº Controle");
        $crud->display_as("codraca", "Raça");
        $crud->display_as("codcategoria", "Categoria");
        $crud->display_as("dtnascimento", "Nascimento");
        $crud->display_as("sexo", "Sexo");
        $crud->display_as("pesonascimento", "Peso Nasc");
        $crud->display_as("codpelagem", "Pelagem");
        $crud->display_as("codcriador", "Criador");
        $crud->display_as("codpai", "Pai");
        $crud->display_as("codmae", "Mãe");
        $crud->display_as("codfazenda", "Fazenda");
        $crud->display_as("status", "Status");
        $crud->display_as("desmamado", "Desmamado");
        $crud->display_as("dataDesmame", "Data de Desmame");

        
        $crud->required_fields("ncontrole", "codraca", "codcategoria", "sexo", "dtnascimento", "codpelagem", "codcriador","codfazenda","status");
        
        $crud->unique_fields('ncontrole');
        
        $lista_status = array('M' => 'M', 'F' => 'F');
        $crud->field_type('sexo', 'dropdown', $lista_status);
        $crud->set_relation("status", "cadstatus", "nome");
        $crud->set_relation("codraca", "cadraca", "nome", array('status' => '1'));
        $crud->set_relation("codpelagem", "cadpelagem", "cor", array('status' => '1'));
        $crud->set_relation("codcriador", "cadcriador", "nome", array('status' => '1'));
        $crud->set_relation("codcategoria", "cadcategoria", "nome", array('status' => '1'));
        $crud->set_relation('codpai','cadgado','nome',array('sexo' => 'M'));
        $crud->set_relation('codmae','cadgado','nome',array('sexo' => 'F'));
        $crud->set_relation('codfazenda','cadfazenda','nome', array('status' => '1'));

        $crud->unset_clone();
        
        //permissoes do usuario
        if ($this->dados_acesso['alterar'] == "0"){
            $crud->unset_edit();
        }
        if ($this->dados_acesso['deletar'] == "0"){
            $crud->unset_delete();
        }
        
        $output = $crud->render();    
        
        $this->template->load("layout/painel", "animal/viewAnimal", (array) $output);
    }
    
    public function categorias(){
        $crud = new grocery_CRUD();
        
        $crud->set_table('cadcategoria');
        $crud->set_subject("Categoria");
        $crud->columns('nome');
        $crud->fields('codcategoria','nome','status');
        
        $crud->display_as("codcategoria", "ID");
        $crud->display_as("nome", "Nome");
        $crud->display_as("status", "Status");
        
        $crud->unique_fields('nome');
        
        $crud->required_fields("nome", "status");
        
        $crud->set_relation("status", "cadstatus", "nome");
        
        $crud->unset_clone();
        
        //permissoes do usuario
        if ($this->dados_acesso['alterar'] == "0"){
            $crud->unset_edit();
        }
        if ($this->dados_acesso['deletar'] == "0"){
            $crud->unset_delete();
        }
        
        $output = $crud->render();    
        
        $this->template->load("layout/painel", "animal/viewAnimal", (array) $output);
    }
    
    public function racas(){
        $crud = new grocery_CRUD();
        
        $crud->set_table('cadraca');
        $crud->columns('nome');
        $crud->fields('codraca','nome','status');
        
        $crud->display_as("codraca", "ID");
        $crud->display_as("nome", "Nome");
        $crud->display_as("status", "Status");
        
        $crud->unique_fields('nome');
        
        $crud->required_fields("nome", "status");
        
        $crud->set_relation("status", "cadstatus", "nome");
        
        $crud->unset_clone();
        
        //permissoes do usuario
        if ($this->dados_acesso['alterar'] == "0"){
            $crud->unset_edit();
        }
        if ($this->dados_acesso['deletar'] == "0"){
            $crud->unset_delete();
        }
        
        $output = $crud->render();    
        
        $this->template->load("layout/painel", "animal/viewAnimal", (array) $output);
    }
    
    public function cores(){
        $crud = new grocery_CRUD();
        
        $crud->set_table('cadcor');
        $crud->columns('nome');
        $crud->fields('codcor','nome','status');
        
        $crud->display_as("codcor", "ID");
        $crud->display_as("nome", "Nome");
        $crud->display_as("status", "Status");
        
        $crud->unique_fields('nome');
        
        $crud->required_fields("nome", "status");
        
        $crud->set_relation("status", "cadstatus", "nome");
        
        $crud->unset_clone();
        
        //permissoes do usuario
        if ($this->dados_acesso['alterar'] == "0"){
            $crud->unset_edit();
        }
        if ($this->dados_acesso['deletar'] == "0"){
            $crud->unset_delete();
        }
        
        $output = $crud->render();    
        
        $this->template->load("layout/painel", "animal/viewAnimal", (array) $output);
    }
    
    public function pelagens(){
        $crud = new grocery_CRUD();
        
        $crud->set_table('cadpelagem');
        $crud->columns('cor');
        $crud->fields('codpelagem','cor','status');
        
        $crud->display_as("codpelagem", "ID");
        $crud->display_as("cor", "Cor");
        $crud->display_as("status", "Status");
        
        $crud->unique_fields('cor');
        
        $crud->required_fields("cor", "status");
        
        $crud->set_relation("status", "cadstatus", "nome");
        $crud->set_relation("cor", "cadcor", "nome");
        
        $crud->unset_clone();
        
        //permissoes do usuario
        if ($this->dados_acesso['alterar'] == "0"){
            $crud->unset_edit();
        }
        if ($this->dados_acesso['deletar'] == "0"){
            $crud->unset_delete();
        }
        
        $output = $crud->render();    
        
        $this->template->load("layout/painel", "animal/viewAnimal", (array) $output);
    }
    
    public function Inseminacoes(){
        $crud = new Grocery_CRUD();
        
        $crud->set_table("cadInseminacao");
        $crud->set_subject("Inseminação");
        
        $crud->columns('data','nVaca','nTouro','horaLA','qtdDose','responsavel','obs');
        
        $crud->display_as("data", "Data");
        $crud->display_as("nVaca", "Numero Vaca");
        $crud->display_as("nTouro", "Numero Touro");
        $crud->display_as("horaLA", "Hora LA");
        $crud->display_as("qtdDose", "Qtd Dose");
        $crud->display_as("responsavel", "Responsável");
        $crud->display_as("obs", "Observação");
        
        $crud->required_fields("data","nVaca","nTouro","responsavel");
        
        $crud->unset_clone();
        
        //permissoes do usuario
        if ($this->dados_acesso['alterar'] == "0"){
            $crud->unset_edit();
        }
        if ($this->dados_acesso['deletar'] == "0"){
            $crud->unset_delete();
        }
            
        $crud->set_relation("responsavel", "cadFuncionario", "nome");
        $crud->set_relation("nVaca", "cadgado", "ncontrole",array('sexo' => 'F'));
        $crud->set_relation("nTouro", "cadgado", "ncontrole",array('sexo' => 'M'));

        
        $output = $crud->render();
        $this->template->load("layout/painel", "animal/viewAnimal", (array) $output);
    }
    
    public function PreSuf(){
        $crud = new Grocery_CRUD();
        
        $crud->set_table("cadSufPre");
        $crud->set_subject("Prefixo/Sufixo");
        
//        $crud->columns("");
        
        $crud->display_as("codCri", "Criador");
        $crud->display_as("codFaz", "Fazenda");
        $crud->display_as("codRaca", "Raça");
        $crud->display_as("codCategoria", "Categoria");
        $crud->display_as("preMacho", "Prefixo Macho");
        $crud->display_as("sufMacho", "Sufixo Macho");
        $crud->display_as("preFemea", "Prefixo Femea");
        $crud->display_as("sufFemea", "Sufixo Femea");
        
        $crud->required_fields('codCri','codFaz','codRaca','codCategoria');
        $crud->unset_clone();
        
        //permissoes do usuario
        if ($this->dados_acesso['alterar'] == "0"){
            $crud->unset_edit();
        }
        if ($this->dados_acesso['deletar'] == "0"){
            $crud->unset_delete();
        }
        
        $crud->set_relation("codCri", "cadcriador", "nome");
        $crud->set_relation("codFaz", "cadfazenda", "nome");
        $crud->set_relation("codRaca", "cadraca", "nome");
        $crud->set_relation("codCategoria", "cadcategoria", "nome");
        


        
        $output = $crud->render();
        $this->template->load("layout/painel", "animal/viewAnimal", (array) $output);
    }
}

