<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {
    
    public function __construct() {
            parent::__construct();
            
    }
    
    public function index(){
        $this->template->load("layout/painel","home/home", array());
    }
}