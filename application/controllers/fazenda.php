<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class fazenda extends CI_Controller {
    
        public function __construct() {
            parent::__construct();
            //identifica qual aplicação esta tentando acessar
            $aplicacao = '/'.$this->router->fetch_class().'/'.$this->router->fetch_method();
            $this->dados_acesso = $this->usuariopermissao->verificarAcesso($aplicacao);
        }
        
        public function fazendas(){
            $crud = new grocery_CRUD();
        
            $crud->set_table('cadfazenda');
            $crud->columns('nome','cnpj','municipio','uf','telefone','email');
            
            $crud->set_subject("Fazenda");
            
            $crud->unique_fields('cnpj');
            
            $crud->display_as("nome", "Nome");
            $crud->display_as("cnpj", "CNPJ");
            $crud->display_as("endereco", "Endereço");
            $crud->display_as("municipio", "Municipio");
            $crud->display_as("uf", "UF");
            $crud->display_as("cep", "CEP");
            $crud->display_as("telefone", "Telefone");
            $crud->display_as("email", "E-mail");
            $crud->display_as("dtcadastro", "Cadastro");
            $crud->display_as("status", "Status");
            
            $crud->required_fields("nome","cnpj","endereco","municipio","uf","cep","telefone","email","dtcadastro","status");
                      
            $crud->set_relation("status", "cadstatus", "nome");
            $crud->set_relation("municipio", "municipios", "nome");
            $crud->set_relation("uf", "estados", "uf");
            
            $crud->field_type("cnpj", "integer");
            $crud->field_type("cep", "integer");
            $crud->field_type("telefone", "integer");
            
            $crud->unset_clone();
            
            //permissoes do usuario
            if ($this->dados_acesso['alterar'] == "0"){
                $crud->unset_edit();
            }
            if ($this->dados_acesso['deletar'] == "0"){
                $crud->unset_delete();
            }

            $output = $crud->render();
            $this->template->load("layout/painel", "fazendas/cadastroFazendas", (array) $output);
        }
        
       
        public function Funcionarios(){
            $crud = new Grocery_CRUD();
            
            $crud->set_table("cadFuncionario");
            $crud->set_subject("Funcionário");
            
            $crud->unique_fields("cpf");
            
            $crud->columns('nome','cpf','rg','dtnascimento','telefone','email');
            
            $crud->display_as("nome", "Nome");
            $crud->display_as("cpf", "CPF");
            $crud->display_as("rg", "RG");
            $crud->display_as("dtnascimento", "Data de Nascimento");
            $crud->display_as("telefone", "Telefone");
            $crud->display_as("email", "E-mail");
            
            $crud->required_fields("nome","cpf","dtnascimento","telefone");
            
            $crud->field_type("cpf", "integer");
            $crud->field_type("telefone", "integer");
            
            $crud->unset_clone();
            
            //permissoes do usuario
            if ($this->dados_acesso['alterar'] == "0"){
                $crud->unset_edit();
            }
            if ($this->dados_acesso['deletar'] == "0"){
                $crud->unset_delete();
            }
            
            $crud->set_relation("status", "cadstatus", "nome");
            $crud->set_relation("codFazenda", "cadfazenda", "nome");
            
            $output = $crud->render();
            $this->template->load("layout/painel", "fazendas/cadastroFazendas", (array) $output);
        }
}
