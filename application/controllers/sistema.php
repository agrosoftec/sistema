<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sistema extends CI_Controller {
    
        public function __construct() {
            parent::__construct();
            //identifica qual aplicação esta tentando acessar
//            $aplicacao = '/'.$this->router->fetch_class().'/'.$this->router->fetch_method();
//            $this->dados_acesso = $this->usuariopermissao->verificarAcesso($aplicacao);
        }
        
        public function aplicacoes(){
            $crud = new Grocery_CRUD();
            
            $crud->set_table("cadaplicacoes");
            $crud->set_subject("Aplicação");
                        
            $crud->columns('codAplicacao','descricao','link','dataCadastro','ativo');
            $crud->fields('descricao','link','dataCadastro','ativo');
            $crud->required_fields('descricao','link','datacadastro','ativo');
            
            $crud->display_as("descricao", "Descrição");
            $crud->display_as("link", "Link");
            $crud->display_as("dataCadastro", "Cadastro");
            $crud->display_as("ativo", "Status");
                        
            $crud->set_relation("ativo", "cadstatus", "nome");
            
            $crud->unset_clone();
            
            //permissoes do usuario
//            if ($this->dados_acesso['alterar'] == "0"){
//                $crud->unset_edit();
//            }
//            if ($this->dados_acesso['deletar'] == "0"){
//                $crud->unset_delete();
//            }
        
            $output = $crud->render();    
        
            $this->template->load("layout/painel", "sistema/viewAplicacao", (array) $output);
        }
        
}