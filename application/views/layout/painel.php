<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Agrosoftec</title>

        <!-- CSS Files -->
        <link href="<?= base_url();?>layout_assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?= base_url();?>layout_assets/css/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="<?= base_url();?>layout_assets/css/demo.css" rel="stylesheet" />
        <script src="<?= base_url();?>layout_assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
    </head>

    <body>
        <div class="wrapper">
            <div class="sidebar" data-image="<?= base_url();?>layout_assets/img/sidebar-5.jpg" data-color="green">
                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="#" class="simple-text">
                            Agrosoftec
                        </a>
                    </div>
                    <ul class="nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="<?= base_url();?>">
                                <i class="nc-icon nc-grid-45"></i>
                                <p>Home</p>
                            </a>
                        </li>
                        <li class="dropdown nav-item">
                            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                <i class="nc-icon nc-square-pin"></i>
                                <span class="notification">Fazendas</span>
                                <span class="d-lg-none">Fazendas</span>
                            </a>
                            <ul class="dropdown-menu">
                                <a class="dropdown-item" href="<?= base_url();?>fazenda/fazendas">Fazendas</a>
                                <a class="dropdown-item" href="<?= base_url();?>fazenda/funcionarios">Funcionários</a>
                            </ul>
                        </li>
                        <li>
                            <a class="nav-link" href="<?= base_url();?>criador/criadores">
                                <i class="nc-icon nc-single-02"></i>
                                <p>Criadores</p>
                            </a>
                        </li>
                        <li class="dropdown nav-item">
                            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                <i class="nc-icon nc-simple-add"></i>
                                <span class="notification">Animais</span>
                                <span class="d-lg-none">Animais</span>
                            </a>
                            <ul class="dropdown-menu">
                                <a class="dropdown-item" href="<?= base_url();?>animal/categorias">Categorias</a>
                                <a class="dropdown-item" href="<?= base_url();?>animal/cores">Cores Pelagem</a>
                                <a class="dropdown-item" href="<?= base_url();?>animal/pelagens">Pelagens</a>
                                <a class="dropdown-item" href="<?= base_url();?>animal/racas">Raças</a>
                                <a class="dropdown-item" href="<?= base_url();?>animal/animais">Animais</a>
                                <a class="dropdown-item" href="<?= base_url();?>animal/inseminacoes">Inseminações</a>
                                <a class="dropdown-item" href="<?= base_url();?>animal/presuf">Prefixos e Sufixos</a>
                            </ul>
                        </li>
                        <li class="dropdown nav-item">
                            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                <i class="nc-icon nc-key-25"></i>
                                <span class="notification">Usuários</span>
                                <span class="d-lg-none">Usuários</span>
                            </a>
                            <ul class="dropdown-menu">
                                <a class="dropdown-item" href="<?= base_url();?>usuario/grupos">Grupo de Usuários</a>
                                <a class="dropdown-item" href="<?= base_url();?>usuario/usuarios">Usuários</a>
                                <a class="dropdown-item" href="<?= base_url();?>usuario/permissaoGrupo">Permissões de Usuários</a>
                            </ul>
                        </li>
                        <li class="dropdown nav-item">
                            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                <i class="nc-icon nc-settings-90"></i>
                                <span class="notification">Sistema</span>
                                <span class="d-lg-none">Sistema</span>
                            </a>
                            <ul class="dropdown-menu">
                                <a class="dropdown-item" href="<?= base_url();?>sistema/aplicacoes">Aplicações</a>
                            </ul>
                        </li>
                     

                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                    <div class=" container-fluid  ">
                        <a class="navbar-brand" href="#pablo"> Sistema </a>
                        <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar burger-lines"></span>
                            <span class="navbar-toggler-bar burger-lines"></span>
                            <span class="navbar-toggler-bar burger-lines"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end" id="navigation">

                            <ul class="navbar-nav ml-auto">

                                <li class="nav-item">
                                    <a class="nav-link" href="<?= base_url();?>login/sair">
                                        <span class="no-icon">Sair</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="content">
                    <?php echo $contents; ?>
                </div>
                <footer class="footer">
                    <div class="container">
                        <nav>

                            <p class="copyright text-center">
                                ©
                                <script>
                                    document.write(new Date().getFullYear())
                                </script>
                                <a href="#">Agrosoftec</a>, Gestão de fazendas
                            </p>
                        </nav>
                    </div>
                </footer>
            </div>
        </div>
    </body>
    <!--   Core JS Files   -->

    <script src="<?= base_url();?>layout_assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>layout_assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
    <script src="<?= base_url();?>layout_assets/js/plugins/bootstrap-switch.js"></script>
    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
    <!--  Chartist Plugin  -->
    <script src="<?= base_url();?>layout_assets/js/plugins/chartist.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="<?= base_url();?>layout_assets/js/plugins/bootstrap-notify.js"></script>


</html>