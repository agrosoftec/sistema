<?php

class UsuarioPermissao {
    
    public function __construct(){
        $this->CI = &get_instance(); //chama as funções de banco e sessão
    }
    
    public function verificarAcesso($aplicacao) {       
        if(!$this->CI->session->userdata("usuario")){ //se a sessão do usuario estiver vazia volta para tela de login
            redirect('login');
        } else {
            //localiza codigo da aplicação 
            
            $grupo_usuario = $this->CI->session->userdata("usuario")['grupo']; //pega o grupo do usuario logado
//            echo $grupo_usuario;
//            echo ' - ' . $aplicacao;
//            exit;
//            $query = $this->CI->db->query("call sys_acessousuario_sp($grupo_usuario,'$aplicacao')");
//            $resultado = (json_decode(json_encode($query->result()), true));
//            
//            $dados_acesso = array();
//            
//            $dados_acesso['acessar'] = $resultado[0]['acessa'];
//            $dados_acesso['alterar'] = $resultado[0]['altera'];
//            $dados_acesso['deletar'] = $resultado[0]['deleta'];
            $dados_acesso['acessar'] = '1';
            $dados_acesso['alterar'] = '1';
            $dados_acesso['deletar'] = '1';
            
            $this->CI->db->close();
            
            if ($dados_acesso['acessar'] != "1"){ 
                redirect('home'); // se usuario não poder acessar a aplicação, é redirecionado para home
            } else {
                return $dados_acesso;
            }
        }
    }
    
    public function verficarPermissoes($aplicacao) {       
        if(!$this->CI->session->userdata("usuario")){ //se a sessão do usuario estiver vazia volta para tela de login
            redirect('login');
        } else {
            $grupo_usuario = $this->CI->session->userdata("usuario")['grupo']; //pega o grupo do usuario logado
            
            $this->CI->db->where("grupo", $grupo_usuario);
            $this->CI->db->where("ativo", "1");
            $this->CI->db->where("aplicacao", $aplicacao);
            
            $permissao = $this->CI->db->get("cadpermissao")->row_array(); //busca na tabela de permissao pelo grupo e aplicação se o usuario pode acessar
            
            return $permissao;
        }
    }
}
